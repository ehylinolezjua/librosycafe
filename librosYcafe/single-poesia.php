<div class="publicaciones">
		<div class="container">

	
			<div class="row">
					<?php $postid = get_the_ID(); ?>
							<?php
								$categoria_poesia = new WP_Query(array(
									'category_name' => 'poesia',
									'showposts' => '4',
									'post__not_in' => array($postid)
									)); 
								while ( $categoria_poesia->have_posts() ) : $categoria_poesia->the_post();
					?>
							
							
			<div class="col s12 m3 l3">
            <article>
             
                  <div class="card ">
                      <div class="card-image">
                            <a href="<?php the_permalink(); ?>">
                            <?php the_post_thumbnail('full', array('class' => 'responsive-img')); ?>
                          </a>
                      </div>
                      <div class="card-stacked">
                          <div class="card-content conten">
                               <h3 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></h3></a>
                              <?php custom_length_excerpt(20); ?>
                          </div>
                          <div class="card-action">
                              <a href="<?php the_permalink(); ?>" class="btn waves-effect   deep-orange-text ">Leer más</a>
                          </div> 
                      </div>
                    </div>  
               
              </article>
               </div>
              
								
							 
								<?php endwhile;
								wp_reset_postdata();
								?>
			</div>
	</div>
</div>
</div>