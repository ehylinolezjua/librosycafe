<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes"/>
  <title>Entre libros y café</title>
  <meta name="description" content="Un portal para escritores independientes y aficionados a la lectura." />  
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/materialize.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/main.css">
  <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body class="body">
  <header class="header">
      <div class="info  orange darken-4">
          <div class="container">
             <div class="row">
                <div class="top-nav right">
                    <ul class="redes">
                        <li><a href="http://www.facebook.com/entre-libros-y-cafe-ve"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="http://www.instagram.com/entre-libros-y-cafe"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="http://www.twitter.com/entre-libros-y-cafe"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
          </div>
      </div>    
      <nav class="nav">
          <div class="container">
              <div class="nav-wrapper">
                  <a href="index.php" class="brand-logo">
                      <img id="logo"  src="<?php bloginfo('template_url');?>/img/logo1.png" alt="logo"/>
                      <img id="logo"  src="<?php bloginfo('template_url');?>/img/logo2.png" alt="logo"/>
                  </a>
                  <a href="#" data-activates="mobile-demo" class="button-collapse">
                    <i class="material-icons">menu</i></a>
                        <ul class="right hide-on-med-and-down">
                          
                              
                              
                                 
                               
                            <?php
                              wp_nav_menu( array( 'theme_location' => 'menu-principal' )); 
                            ?>
                         
                              
                            
                        </ul>
                    <ul class="side-nav" id="mobile-demo">
                      <?php
                              wp_nav_menu( array( 'theme_location' => 'menu-principal' )); 
                            ?>
                      
                    </ul>
              </div>
            </div>
        </nav>
  </header>