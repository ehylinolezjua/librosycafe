 <section class="publicaciones-recientes">
      <div class="container">
        
            
                <h2 class="section-titles">Publicaciones recientes</h2>
                <div class="row">
            
              <?php $postid = get_the_ID(); ?>
                      <?php
                       $categoria_publicaciones_recientes = new WP_Query(array(
                          'category_name' => 'publicaciones-recientes',
                            'showposts' => '3',
                              'post__not_in' => array($postid)
                             )); 
                         while ( $categoria_publicaciones_recientes->have_posts() ) : $categoria_publicaciones_recientes->the_post();
                        ?>

            <div class="col s12 m4">
            <article>
              
                <div class="card medium">
                  <div class="card-image">
                    <a href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('prs', array('class' => 'img-responsive')); ?>
                     </a> 
                  </div>
                  <div class="card-content">
                    <h3 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
                      <?php custom_length_excerpt(20); ?>
                  </div>
                  <div class="card-action">
                      <a href="<?php the_permalink(); ?>" class="btn waves-effect   deep-orange-text ">Leer más</a>
                  </div> 
                </div>
              
            </article>
            </div>
            <?php endwhile;
                wp_reset_postdata();
                ?>
            
      </div>
    </div>
  </section>