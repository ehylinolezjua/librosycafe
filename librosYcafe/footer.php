 <footer class="footer">
    <section class="banner-footer">
        <div>
          <div class="container"> 
            <div class="publicaciones-recientes-footer"> 
              <div class="row">
                <div class="col s12">
                <h3 class="titulo-footer">Publicaciones recientes</h3>
                </div>
                <?php $postid = get_the_ID(); ?>
                    <?php
                      $categoria_publicaciones_recientes = new WP_Query(array(
                      'category_name' => 'publicaciones-recientes',
                      'showposts' => '4',
                      'post__not_in' => array($postid)
                      )); 
                      while ( $categoria_publicaciones_recientes->have_posts() ) : $categoria_publicaciones_recientes->the_post();
                    ?>
                <article>
                  <div class="col s12 m3">
                    <div class="card">
                      <div class="card-image">
                        <?php the_post_thumbnail('cafe', array('class' => 'img-responsive')); ?>
                        <h4 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h4>
                      </div>
                    </div>
                  </div>
              </article>
              <?php endwhile;               
                ?>
        </div>
          <div class="row">
            <div class="col s12 m4">
              <div class="menu-redes">
                  <h3 class="titulo-footer">Síguenos en</h3>
                  <ul>
                    <li><a href="http://www.facebook.com/entre-libros-y-cafe"><i class="fa fa-facebook"></i>
                    </a></li>
                    <li><a href="http://www.instagram.com/entrelibrosycafeve"><i class="fa fa-instagram"></i>
                    </a></li>
                    <li><a href="http://www.twitter.com/entre-libros-y-cafe"><i class="fa fa-twitter"></i>
                    </a></li>
                  </ul>
                </div>
              </div>
                <div class="col s12 m4">
                    <h3 class="titulo-footer">Escribenos a</h3>
                    <ul>
                      <li><a class="email" href="#"><i class="fa fa-envelope"></i>  entrelibrosycafeve@gmail.com</a></li>
                    </ul>
                </div>
                <div class="col s12 m4">
                    <figure>
                      <a href="index.php">
                        <img id="logo"  src="<?php bloginfo('template_url');?>/img/logo1.png" height="100" width="150" alt="logo"/>
                      <img id="logo"  src="<?php bloginfo('template_url');?>/img/logo2.png" height="100" width="150" alt="logo"/>
                      </a>
                    </figure>
                </div>
          </div>
        </div>
      </div>
    </section>
    <div class="ultima-linea white-text orange darken-4 center">
        <div class="container">
            <p>ENTRE LIBROS Y CAFÉ © 2016. TODOS LOS DERECHOS RESERVADOS.<p>
        </div>
    </div>
    <span class="ir-arriba"><i class="material-icons">keyboard_arrow_up</i></span>
  </footer>
  </main>
  <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/materialize.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/main.js"></script>
</body>
</html>