<section class="delirios">
  <div class="container">
    <div class="row">
       <ul class="collapsible popout" data-collapsible="accordion">
        
          <li>
            
            <div class="collapsible-header">Delirios Romanticos</div>
            <?php 
                $deliriosr = new WP_Query(array(
                'category_name' => 'delirios_romanticos',
                'showposts' => '2',
                )); 
                while ( $deliriosr->have_posts() ) : $deliriosr->the_post();
              ?>
            
            <div class="collapsible-body">
              <h3 class="card-title center"><?php the_title();?></h3>
              <?php the_content(); ?></div>
            <?php endwhile;
                  wp_reset_postdata();
                ?>
          </li>
          
          <li>
            <div class="collapsible-header">Delirios Drama</div>
            
            <?php 
                $deliriosd = new WP_Query(array(
                'category_name' => 'delirios_drama',
                'showposts' => '1',
                )); 
                while ( $deliriosd->have_posts() ) : $deliriosd->the_post();
              ?>
      
            
            <div class="collapsible-body">
              <h3 class="card-title center"><?php the_title();?></h3>
              <?php the_content(); ?>
            </div>
      
           
            <?php endwhile;
              wp_reset_postdata();
            ?>
           
          </li>
          <li>
            
            <div class="collapsible-header">Delirios Fantasía</div>
              <?php 
                $deliriosf = new WP_Query(array(
                'category_name' => 'delirios_fantasia',
                'showposts' => '2',
                )); 
                while ( $deliriosf->have_posts() ) : $deliriosf->the_post();
              ?>

              <div class="collapsible-body">
              <h3 class="card-title center"><?php the_title();?></h3>
              <?php the_content(); ?>
            </div>
            
            <?php endwhile;
              wp_reset_postdata();
              ?>
          </li>
        </ul>
    </div>
  </div>
</section>