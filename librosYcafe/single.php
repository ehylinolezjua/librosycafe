<?php get_header(); ?>
	<main class="page-single">	
	<div class="container">
		<div class="row">
					<?php
						while ( have_posts() ) : the_post();
					?>

							<div class="col s12">
								<article>
									 <div class="card medium-single">
					                  <div class="card-image">
					                      <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
					                      
					                  </div>
					                   <div class="card-content">

                  			<?php the_time('l, F jS, Y') ?> 
                    <h3 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
                      <?php the_content(); ?><br>
                      <?php the_tags(); ?> <br>
                  </div>
                  			
								</article>
								<?php comments_template(); ?>
							</div>
							
						<?php endwhile;
						?>
							</div>
	</div>
					<div class="col s12">
						
						<?php
							if (in_category('publicaciones-recientes'))
							{
								include("publicaciones.php");
							} else if (in_category('mas_leidos'))
							{
								include("leer-mas.php"); 
							} else if (in_category('relatos'))
							{
								include("single-relatos.php");	
							}else if (in_category('poesia'))
							{
								include("single-poesia.php");
							}else if (in_category('drama'))
							{
								include("single-drama.php");
							}else if (in_category('fantasia'))
							{
								include("single-fantasia.php");
							}else if (in_category('ficcion'))
							{
								include("single-ficcion.php");
							}else if (in_category('romance'))
							{
								include("single-romance.php");
							}else if (in_category('terror'))
							{
								include("single-terror.php");
							}
								 
						?>
					</div>
					
	
<?php get_footer(); ?>
