<section class="relatos">
    <div class="container">
        <div class="row">
            <div class="col s12">
              <h2 class="section-titles">Relatos</h2>
            </div>



            <?php $postid = get_the_ID(); ?>
            <?php
              $categoria_relatos = new WP_Query(array(
                'category_name' => 'relatos',
                'showposts' => '3',
                'post__not_in' => array($postid)
                )); 
              while ( $categoria_relatos->have_posts() ) : $categoria_relatos->the_post();
        ?>
                <article class="articulo">
                  <div class="col s12 m12">
                    <div class="card horizontal">
                        <div class="card-image">
                          <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                        </div>
                        <div class="card-stacked">
                            <div class="card-content">
                              <h3 class="horizontal-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
                              <?php custom_length_excerpt(20); ?>
                            </div>
                            <div class="card-action">
                            <a href="<?php the_permalink(); ?>" class="btn waves-effect   deep-orange-text ">Leer más</a>
                            </div> 
                        </div>
                    </div>  
                  </div>
                </article>
                
                <?php endwhile;
            wp_reset_postdata();
          ?>
          
        </div>
      </div>
  </section> 