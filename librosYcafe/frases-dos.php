<div class="container">
<div class="frases-dos">
          <div class="row">
            <?php 
                  $frases_dos = new WP_Query(array(
                    'category_name' => 'frases-dos',
                    'showposts' => '2',
                    )); 
                  while ( $frases_dos->have_posts() ) : $frases_dos->the_post();
                ?>
            <article class="articulo">
                <div class="col s12 m6">
                  <div class="card ">
                      <div class="card-image">
                        <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                      </div>
                      <div class="card-stacked">
                          <div class="card-content">
                             <h3 class="card-title"><?php the_title(); ?></h3>
                            <?php custom_length_excerpt(20); ?>
                          </div>
                      </div>
                  </div>  
                </div>
            </article>
            <?php endwhile; wp_reset_postdata(); ?>
            
          </div>
    </div>
  </div>