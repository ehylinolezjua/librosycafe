<?php
if( function_exists( 'add_theme_support' ) )
	add_theme_support( 'post-thumbnails' );
	add_image_size('cafe', 100, 71,false);
	add_image_size('prs', 263, 188,false);
	add_image_size('pr', 300, 200,false);
	add_image_size('caf', 500, 350,false);

	function custom_length_excerpt($word_count_limit) {
		$content = wp_strip_all_tags(get_the_content() , true );
		echo wp_trim_words($content, $word_count_limit);
	}

if(function_exists('register_nav_menus'))
   	{
      register_nav_menus(
        array(
              'menu-principal' => __('Botonera Principal'),
             )
      );
   }
   
if(function_exists("register_sidebar"))
	{
		register_sidebar(array(
			'name' => 'Copyright',
			'id' => 'copyright-widget',
			'description' => 'Coloque el copyright'
	));
	}

?>