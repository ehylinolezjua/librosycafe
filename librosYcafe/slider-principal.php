<main class="page-inicio">
    <section class="section-slider">
      <div class="slider fullscreen">
        <ul class="slides">
            <li>
              <img class="responsive-img" src="<?php bloginfo('template_url');?>/img/pc-cafe.jpg" alt="slider "> 
              <div class="caption center-align">
                  <h3 class="slider-titulo"><span class="slider-span">Para escritores independientes</span>     
                  <span class="slider-span">Y aficionados a la lectura<span></h3>
                    <a href="registro.php" class="btn waves-effect  white-text darken-text-2">Registro</a>
                    <a href="acceso.php" class="btn waves-effect white-text darken-text-2">Iniciar sesión</a>
              </div>
            </li>
            <li>
                <img class="responsive-img" src="<?php bloginfo('template_url');?>/img/slider-biblioteca.jpg" alt="slider "> 
              <div class="caption center-align">
                <h1 class="slider-titulo">Entre libros y café</h1>
                  <a href="" class="btn waves-effect white-text darken-text-2">Registro</a>
                  <a href="" class="btn waves-effect white-text darken-text-2">Iniciar sesión</a>
              </div>
            </li>
            <li>
              <img class="responsive-img" src="<?php bloginfo('template_url');?>/img/slider-escala.jpg" alt="slider "> 
              <div class="caption center-align">
                <h3 class="slider-titulo">Explora nuestras categorias</h3>
                  <a href="" class="btn waves-effect white-text darken-text-2">Registro</a>
                  <a href="" class="btn waves-effect white-text darken-text-2">Iniciar sesión</a>
              </div>
            </li>
            <li>
              <img class="responsive-img" src="<?php bloginfo('template_url');?>/img/slider-home.jpg" alt="slider "> 
              <div class="caption center-align">
                <h3 class="slider-titulo"><span class="slider-span">Para todos los usuarios</span>
                  <span class="slider-span">Escribe, lee , interactua, comenta<span></h3>
                  <a href="" class="btn waves-effect white-text darken-text-2">Registro</a>
                  <a href="" class="btn waves-effect white-text darken-text-2">Iniciar sesión</a>
              </div>
            </li>
        </ul>
      </div>
  </section>