<section class="banner">
      <div class="parallax-container">
          <div class="parallax center">
              <?php 
                  $banner = new WP_Query(array(
                    'category_name' => 'banner'
                    )); 
                  while ( $banner->have_posts() ) : $banner->the_post();
                ?>
                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                  <div class="container">
                    <h2 class="header"><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                </div>
              <?php endwhile; wp_reset_postdata(); ?>
          </div> 
      </div>
  </section>